FROM phusion/baseimage:0.11

CMD ["/sbin/my_init"]
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# Install python3.6 & pip
RUN apt-get update && \
    apt-get install -y python3.6 && \
    apt-get install -y python3-pip && \
    apt-get clean;

RUN apt-get update && \
   apt-get install -y openjdk-8-jdk && \
   apt-get install -y ant && \
   apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

#RUN pip3 install stanfordcorenlp && \
#    pip3 install neuralcoref && \
#    pip3 install spacy==2.0.13 && \
#    python3 -m spacy download en && \
#    pip3 install cython && \
#    pip3 install pytest && \
#    pip3 install https://github.com/huggingface/neuralcoref-models/releases/download/en_coref_lg-3.0.0/en_coref_lg-3.0.0.tar.gz;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

# create root directory for our project in the container
RUN mkdir /sealion

# Set the working directory
WORKDIR /sealion

# Copy the current directory contents into the container
ADD . /sealion/

#FROM python:3.6-slim
RUN pip3 install nltk
RUN python3 -c "import nltk; nltk.download('all')"
#ENTRYPOINT python

# Install any needed packages specified in requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt


