from celery import shared_task
from app.celery import app
from django.shortcuts import get_object_or_404
from celery import task, current_task
from trex2.models import Abstract, DPProcess, DPStats
from trex2.DB_operations import get_dp, save_dp
from trex2.process.data_preparation.data_preparation import DataPreparation

@shared_task
def prepare_data(abstract_id):
    #TODO: use batches
    abstracts = Abstract.objects.all()
    for i, abstract in enumerate(abstracts):
        dp = DataPreparation(abstract.text)
        stats = dp.normalize()
        
        p = DPProcess(
            output = stats[0],
            stats = [ DPStats(msg=s[0], applied=s[1]) for s in stats[1] ]
        )
        p.save()
        
        abstract.lexprocess.dp = p
        abstract.lexprocess.dp.save()
        abstract.lexprocess.save()
        '''
        try:
            abstract.processedabstract
        except ProcessedAbstract.DoesNotExist:
            dp = DataPreparation(abstract.text)
            stats = dp.normalize()
            
            pa = ProcessedAbstract(
                processed_text=stats[0],
                dp_stats=[DPStats(msg=s[0], applied=s[1]) for s in stats[1]],
                original=abstract
            )
            abstract.processedabstract = pa
            pa.save()
        processed = abstract.processedabstract
        '''
        process_percent = int(100 * float(i) / float(len(abstracts)))
        
        current_task.update_state(state='PROGRESS',
                                  meta={
                                      'current': process_percent,
                                      'msg': abstract.concept
                                  })

#--------------data preparation process------------------
@shared_task
def data_preparation_process(abstract_id, is_process_all = False):
    #get object from DB with abstract_id
    abstract = get_object_or_404(Abstract, id=abstract_id)
    print("DATA PREPARATION OF", abstract.concept, "--------", abstract.id)
    if abstract.is_done_dp and is_process_all:
        return
    
    if (abstract.is_done_dp == False):
        #calculate and save on DB data preparation
        boundaries_list = save_dp(abstract)
    else:
        #get data preparation from DB
        boundaries_list = get_dp(abstract)
    #coreference works only with more than 16 GIGA of RAM
    # ---------coreference----------
        #coref = Coreference_process()
        #coref_list_spacy = coref.coreference_resolution(document)

        #stanford = stanford_nlp(document)
        #coref_list_stanford = stanford.stanford_process()
    # -----------------------------
    if (is_process_all == False):
        return (boundaries_list, abstract)
    