import re
class SimpleCoreference():

    def __init__(self):
        self.pronoun_list = ["he", "she", "it", "they"]


    def find_and_replace_pronouns(self, text, document_surfaceform):
        new_text = text.strip()

        for pronoun in self.pronoun_list:
            new_text = re.sub(r''+pronoun+'$', document_surfaceform, new_text, flags=re.IGNORECASE)
            new_text = re.sub(r'^'+pronoun+'', document_surfaceform, new_text, flags=re.IGNORECASE)
            new_text = re.sub(r'[^\w]('+pronoun+')[^\w]', " " + document_surfaceform + " ", new_text, flags=re.IGNORECASE)

        return new_text




