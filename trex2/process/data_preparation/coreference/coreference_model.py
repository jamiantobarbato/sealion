#model to store coreference's properties
class Coreference_model:

    def __init__(self,pronoun,coref,boundary):
        #pronoun to replace with coref (example it, he ,she, ecc...)
        self.pronoun = pronoun
        #coref to replace
        self.coref = coref
        #Pronoun's boundary
        self.boundary = boundary