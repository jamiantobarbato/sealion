import spacy
from trex2.process.data_preparation.coreference.coreference_model import Coreference_model

#coreference resolution spacy
class Coreference_process:

    #use coref_lg library to make coreference in the given text
    def __init__(self):
        self.coref_list = []
        self.nlp = spacy.load('en_coref_lg')

    def coreference_resolution(self,document):
        print ("Spacy")
        doc = self.nlp(document.text)
        for token in doc:
            try:
                if token.pos_ == 'PRON' and token._.in_coref:
                    for cluster in token._.coref_clusters:
                        if(token.text.lower() != cluster.main.text.lower()):
                            #create object of coreference
                            coref_instance = Coreference_model(token.text,cluster.main.text,(token.idx,token.idx+len(token.text)))
                            self.coref_list.append(coref_instance)

            except Exception as e:
                print (e)

        return self.coref_list


