import re

#Data Preparation class
class DataPreparation:
    class Pattern:
        def __init__(self, pattern, output):
            self.pattern = pattern
            self.output = output
        #matching patterns using regular expression
        def match(self, text):


            pattern = re.compile(self.pattern)         #crea il pattern
            matches = pattern.finditer(text)           #finditer : ritorna una sequenza di iteratori /// ritorna un iterabile
            boundaries = []

            for match in matches:
                boundaries.append(match.span())        #span : ritorna una tupla con la posizione di inizio e fine

            (result, n) = re.subn(self.pattern, self.output, text)
            #subn ritorna il testo in cui ha sostituito il pattern con l'output con il numero di volte in cui l'ha applicato


            return (result, n, boundaries)
            #result : testo con il pattern applicato
            #n : numero di volte in cui è stato applicato il pattern
            #boundaries : elenco di coppie con i boundaries
    
    norm_functions = [
        #TODO: for strange characters use hexadecimals
        #TODO: make better logs
        #PROPOSAL: should I extract logs out this class? (and put them in the view?)
        (Pattern(r'–', r'-'),                   "translated non standard -"),
        (Pattern(r'—', r'-'),                   "translated non standard -"),
        (Pattern(r'(\w|\W)-\s', r'\1 '),        "removed trailing -"),
        (Pattern(r'[\"\/]', r' '),              "removed bad characters"),
        (Pattern(r'\([^)]*\)|\[[^\]]*\]', r''), "removed round and square brackets"),
        (Pattern(r'\s{2,}', r' '),              "removed double spaces -test-"),
        (Pattern(r'\s+-\s+', r', '),            "translated - to ,"),
        (Pattern(r'(\w)-(\w)', r'\1 \2'),       "removed - between two words"),
        (Pattern(r'\s+(,|\.)', r'\1'),          "removed spaces before commas and dots"),
    ]
    
    def __init__(self, raw_text):
        self.raw_text = raw_text
        self.applied_norm = []
        
    def normalize(self):
        result = self.raw_text

        for nFunc in DataPreparation.norm_functions:
            (result, applied, boundaries) = nFunc[0].match(result)          #### applica il pattern al testo e cicla fino ad applicarli tutti
            if applied > 0:
                self.applied_norm.append((nFunc[1], applied, boundaries))
            
        return (result, self.applied_norm)                                  ##### applied_norm contiene il pattern applicato , il numero di volte che lo ha applicato e una lista dei boundary

#### [Matt]
    def separate_sentence_at_pronoun(self, text):
        new_text = text.strip()
        list_of_sentence = new_text.split('.')
        new_list_of_sentence=[]
        for sentence in list_of_sentence:
            for pronoun in ["he", "she", "it", "they"]:
                sentence=sentence.replace(', ' + pronoun , '. ' + pronoun)
            new_list_of_sentence.append(sentence)
        return '.'.join(new_list_of_sentence)
####


#### [Matt]
    def delete_subsentence_without_coreference(self, text ):
        new_text = text.strip()
        list_of_sentence = new_text.split('.')
        new_list_of_sentence=[]
        for sentence in list_of_sentence:
            triple_commas = re.findall(r'\,\s.*?\,\s.*?\,\s', sentence)
            subsentence = re.findall(r'\,\s.*?\,\s', sentence)
            pronoun_in_subsentence = False
            for pronoun in ['he', 'she', 'it', 'they']:
                if pronoun in str(subsentence):
                    pronoun_in_subsentence = True

            if ((not triple_commas) and (not pronoun_in_subsentence)):
                sentence = re.sub(r'\,\s.*?\,\s', " ", sentence)             #### regex per eliminare il contenuto fra 2 virgole

            new_list_of_sentence.append(sentence)

        return '.'.join(new_list_of_sentence)
####