import SPARQLWrapper

from celery import shared_task
from celery import task, current_task
from trex2.models import Abstract
import os

@shared_task
def addAbstracts():
    
    # read from file all abstract's links
    with open(os.path.join(os.path.dirname(__file__), '../static/import_data/wikipedia-link.txt')) as file:
        for c, f in enumerate(file):
            # replace dbpedia abstract uri with wikipedia
            uri = f.strip().replace("https://en.wikipedia.org/wiki/","http://dbpedia.org/resource/")
            # replace %27 with /'
            if("%27" in uri):
                uri = uri.strip().replace("%27","'")

            try:
                # make SPARQL query to obtain text
                stringQuery = "SELECT ?text where { <"+uri+"> dbo:abstract ?text . FILTER langMatches( lang(?text), 'EN' ) }"

                sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
                sparql.setQuery(stringQuery)
                sparql.setReturnFormat(SPARQLWrapper.JSON)
                results = sparql.query().convert()

                if(len(results['results']['bindings'])==0):
                    print(uri)

                concept = uri.strip().replace("http://dbpedia.org/resource/", "")
                concept = concept.replace("_", " ")
                # parse sparql query
                for i in results['results']['bindings']:
                    text = i['text']['value']

                    # Add to MongoDB the abstract's wikipedia
                    Abstract(
                        uri=uri,
                        concept=concept,
                        text=text,
                        percentage=0
                    ).save()
                
                current_task.update_state(state='PROGRESS',
                                          meta={
                                            'current': None,
                                            'msg': uri
                                          })

            except Exception as e:
                print (e)
            

def addSingleAbstract(uri_String):
    if('https://en.wikipedia.org/wiki/' in uri_String):
        # replace dbpedia abstract uri with wikipedia
        uri = uri_String.strip().replace("https://en.wikipedia.org/wiki/", "http://dbpedia.org/resource/")

        try:
            # make SPARQL query to obtain text
            stringQuery = "SELECT ?text where { <" + uri + "> dbo:abstract ?text . FILTER langMatches( lang(?text), 'EN' ) }"

            sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
            sparql.setQuery(stringQuery)
            sparql.setReturnFormat(SPARQLWrapper.JSON)
            results = sparql.query().convert()
            concept = uri.strip().replace("http://dbpedia.org/resource/", "")
            concept = concept.replace("_", " ")
            # parse sparql query
            for i in results['results']['bindings']:
                text = i['text']['value']


                # Add to MongoDB the abstract's wikipedia
                Abstract(
                    uri=uri,
                    concept=concept,
                    text=text
                ).save()

        except Exception as e:
            print (e)
    else:
        print("Error")
