import SPARQLWrapper


def query_sparql_to_dbpedia(subject_ontology,object_ontology):

    subject_class = subject_ontology.replace("http://dbpedia.org/ontology/", "dbo:")
    object_class = object_ontology.replace("http://dbpedia.org/ontology/", "dbo:")


    if "dbo" in object_class:
        stringQuery = '''select distinct ?predicate 
            where { 
                ?subject rdf:type ''' + subject_class + ''' . 
                ?object  rdf:type ''' + object_class + ''' . 
                ?subject ?predicate ?object.
                filter(contains(str(?predicate),"ontology")) 
                } limit 50'''
    elif "XMLSchema" in object_class:
            object_class = object_class.replace("%23", "#")
            stringQuery = '''SELECT distinct ?predicate
                WHERE {
                ?subject rdf:type ''' + subject_class + ''' .
                ?subject ?predicate ?object.
                filter(datatype(?object) = <''' + object_class + '''>).
                filter(contains(str(?predicate),"ontology"))
                } limit 50'''
    else:
        return

    #SPARQLWrapper throw a EndPointInternalError Exception
    try:
        sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
        #sparql = SPARQLWrapper.SPARQLWrapper("http://149.132.176.50:8890/sparql")
        sparql.setQuery(stringQuery)
        sparql.setReturnFormat(SPARQLWrapper.JSON)
        results = sparql.query().convert()
    except :
        #print(" stringQuery in caso di errore " + stringQuery)
        return

    return results

def parse_dbpedia_response(response):
    predicate_list = []
    try:
        if (len(response['results']['bindings']) != 0):
            for predicate in response['results']['bindings']:
                if "http://dbpedia.org/ontology" in predicate['predicate']['value']:
                    predicate_list.append(predicate['predicate']['value'])
    except :
        return predicate_list

    return predicate_list


def get_equivalent_class(ontology):
    ontology = 'dbo:' + ontology
    stringQuery = '''select distinct ?equivalent 
        where
        {
        { ''' + ontology + ''' owl:equivalentClass ?equivalent. }
        union
        { ?equivalent owl:equivalentClass ''' + ontology + '''. }
        filter(contains(str(?equivalent),"ontology"))
        }
        '''

    sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(stringQuery)
    sparql.setReturnFormat(SPARQLWrapper.JSON)
    results = sparql.query().convert()

    if(len(results['results']['bindings'])>0):
        return results['results']['bindings'][0]['equivalent']['value']
    else:
        return

