### Models ###

#Sentence_Model Model
class Sentence_model:

    def __init__(self, sentence, predicates = [], triples = []):
        #sentence
        self.sentence = sentence
        #predicates extracted from this sentence
        self.predicates = predicates
        #triple associated to this sentence
        self.triples = triples

#Entity_Sentence Model
class Entity_Sentence:

    def __init__(self,uri, surfaceform, boundary, annotator=[]):
        #uri of entity
        self.uri = uri
        #surfaceform of entity
        self.surfaceform = surfaceform
        #boudary of entity
        self.boundary = boundary
        #annotator of entity
        self.annotator=annotator

#Otology Model
class Ontology:

    def __init__(self, entity_uri, surfaceform, boundary, ontology_list=[], annotator=[]):
        #entity uri
        self.entity_uri = entity_uri
        #surfaceform of entity
        self.surfaceform = surfaceform
        #boundary of entity
        self.boundary = boundary
        #list of classes (dbo:Class) about this entity
        self.ontology_list = ontology_list
        #list of annotator
        self.annotator = annotator

#Couple Model
class Couple:

    def __init__(self, ontology_1, ontology_2, ontology_boundary_1, ontology_boundary_2, uri_1, uri_2, annotator_1=[], annotator_2=[]):
        #class of the first entity (dbo:Class)
        self.ontology_1 = ontology_1
        #class of the second entity (dbo:Class)
        self.ontology_2 = ontology_2
        #boundary of the first entity
        self.ontology_boundary_1 = ontology_boundary_1
        #boundary of the second entity
        self.ontology_boundary_2 = ontology_boundary_2
        #uri first entity
        self.uri_1 = uri_1
        #uri second entity
        self.uri_2 = uri_2
        #annotator of 1st entity
        self.annotator_1 = annotator_1
        #annotator of 2nd entity
        self.annotator_2 = annotator_2

#Triple Model
class Triple:

    def __init__(self, subject_uri, subject_boundary, object_uri, object_boundary, subject_ontology, predicate_uri, object_ontology, subject_annotator, object_annotator):
        #subject entity uri
        self.subject_uri = subject_uri
        #subject boundary
        self.subject_boundary = subject_boundary
        #object entity uri
        self.object_uri = object_uri
        #object boundary
        self.object_boundary = object_boundary
        #subject type class (dbo:Class)
        self.subject_ontology = subject_ontology
        #predicate uri (dbo:Class) "http://dbpedia/ontology ..."
        self.predicate_uri = predicate_uri
        #object type class (dbo:Class)
        self.object_ontology = object_ontology
        #subject string
        self.subject = ""
        #predicate string
        self.predicate = ""
        #object string
        self.object = ""
        #subject annotator
        self.subject_annotator = subject_annotator
        #object annotator
        self.object_annotator = object_annotator

#Predicate Model
class Predicate:

    def __init__(self, predicate, start_char, end_char):
        #predicate
        self.predicate = predicate
        #start char of predicate
        self.start_char = start_char
        #end char of predicate
        self.end_char = end_char
