from celery import shared_task
from django.shortcuts import get_object_or_404
from celery import task, current_task
from trex2.models import Abstract
from trex2.DB_operations import get_le, save_le
import trex2.process.data_preparation.tasks as dpTasks
import trex2.process.entity_linking.tasks as elTasks
from trex2.process.entity_linking.entity_model import Document
from trex2.process.literal_extraction.date_time.date_linker import DateLinker
from trex2.process.literal_extraction.regex.check_text import CheckText, Date_concepts

#--------------literal extraction process------------------
@shared_task
def literal_extraction_process(abstract_id, is_process_all = False):
    #get abstract object from DB
    abstract = get_object_or_404(Abstract, id=abstract_id)
    print("LITERAL EXTRACTION OF", abstract.concept, "--------", abstract.id)
    if abstract.is_done_le and is_process_all:
        return

    if(abstract.is_done_le == False):

        if abstract.is_done_dp == False:
            dpTasks.data_preparation_process(abstract.id, True)
        if abstract.is_done_el == False:
            elTasks.entity_linking_process(abstract.id, True)


        abstractToLink = abstract.lexprocess.dp
        document = Document(abstract.uri, abstract.concept, abstract.uri, abstractToLink.output)
        #create DateLinker object
        date = DateLinker()
        #search date in the text
        document = date.run(document)
        #create object CheckText
        check = CheckText()
        #search Literals in the text
        type = check.check_text(document.text)
        #limit literals (if check finds a literal and it is a date, it will be deleted)
        type = limit_literals(type, document.entities)
        #save on DB literals
        save_le(abstract, document, type)

    else:
        
        #get result from DB
        (document, type) = get_le(abstract)
    
    if (is_process_all == False):
        return (abstract, document, type)

def limit_literals(regex, date):
    regex_final = []

    for reg in regex:
        exist = False
        #if d is in date's list, it will be deleted
        for d in date:
            if (reg.word == d.surfaceform):
                exist = True
        if(exist == False):
            regex_final.append(reg)

    return regex_final
