# -*- coding: utf-8 -*-

import re
import datetime
from trex2.process.literal_extraction.date_time.format_data import *
from trex2.process.entity_linking.entity_model import Entity

# class Date, it is used to store entities
# when they are found in the text
class Date:
    def __init__(self,date_string,boundaries):
        self.date_string=date_string
        self.boundaries=boundaries

# Class that match dates in a piece of text
class DateLinker:

    # init method
    def __init__(self):
        # list of all dates found in the text
        self.dates = []

    def run(self,document):
        # instance of DatePatterns
        datepatterns = DatePatterns()
        # apply all patterns to the text given, and all dates found are added to dates list
        datepatterns.apply_all_patterns(document.text,self.dates)

        # for every date, it is saved like an Entity object
        for date in self.dates:
            if(date.date_string[0]=='-'):
                if len(date.date_string[1:]) == 4:
                    stdform = date.date_string + '-00-00T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                elif len(date.date_string[1:]) == 7:
                    stdform = date.date_string + '-00T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                elif len(date.date_string[1:]) == 10:
                    stdform = date.date_string + 'T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                else:
                    stdform = date.date_string + '^^<http://www.w3.org/2001/XMLSchema#dateTime>'

            else:
                if len(date.date_string) == 4:
                    stdform = date.date_string + '-00-00T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                elif len(date.date_string) == 7:
                    stdform = date.date_string + '-00T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                elif len(date.date_string) == 10:
                    stdform = date.date_string + 'T00:00:00Z^^http://www.w3.org/2001/XMLSchema#dateTime'
                else:
                    stdform = date.date_string + '^^<http://www.w3.org/2001/XMLSchema#dateTime>'

            entity = Entity(
                uri=date.date_string,
                boundaries=date.boundaries,
                surfaceform=document.text[date.boundaries[0]:date.boundaries[1]],
                annotator="DateLinker",
            )
            document.entities.append(entity)
        return document

class DatePatterns:

    def __init__(self):
        self.dates = []

    # function that apply all patters to the text given
    def apply_all_patterns(self, text, dates):
        self.pattern(text, dates)
        self.date_keyword_pattern(text,dates)
        return dates

    # apply pattern on text passed as arguments
    def make_matching(self, text, match_string):
        pattern = re.compile(match_string, re.IGNORECASE)
        matches = pattern.finditer(text)

        return matches


    def pattern(self,text,dates):

        #pattern used to find date in the given text
        pattern = r'\d{1,4}[\,\.\s\t\n]+bc|' \
                  r'\d{4}[-.\/]\d{1,2}[-.\/]\d{1,2}|' \
                  r'\d{1,2}[-.\/]\d{1,2}[-.\/]\d{4}|' \
                  r'\d{1,2}[-.\/]\d{1,2}[-.\/]\d{2}|' \
                  r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{1,2}[\.\,\s\t\n\/]+\d{4}|' \
                  r'\d{1,2}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{4}|' \
                  r'\d{4}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{1,2}|' \
                  r'\d{4}[\.\,\s\t\n\/]+\d{1,2}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)|' \
                  r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\n\t\/]+\d{4}|' \
                  r'\d{4}[\.\,\s\n\t\/]+(january|february|march|april|may|june|july|august|september|october|november|december)|' \
                  r'\d{1,2}(th|st)?[\.\,\s\t\n]+(century|Century)|' \
                  r'[0-9][0-9][0-9]0s|' \
                  r'(summer|winter|spring|autumn)[\.\,\s\t\n]+\d{4}|' \
                  r'\d{4}[\.\,\s\t\n]+(summer|winter|spring|autumn)|' \
                  r'(summer|winter|spring|autumn)[\.\,\s\t\n]+of[\.\,\s\t\n]+\d{4}|' \
                  r'\d{4}[\.\,\s\t\n]+of[\.\,\s\t\n]+(summer|winter|spring|autumn)|' \
                  r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\n\t]+of[\.\,\s\n\t]+\d{4}|' \
                  r'\d{4}[\.\,\s\n\t]+of[\.\,\s\n\t]+(january|february|march|april|may|june|july|august|september|october|november|december)|' \
                  r'\d{4}|' \
                  r'(summer|winter|spring|autumn)|' \
                  r'(monday|tuesday|wednesday|thursday|friday|saturday|sunday)'


        #apply matching
        matches = self.make_matching(text, pattern)

        format_data = FormatData()
        #from the matched data, create the correspondent value
        for match in matches:
            stringa = match.group()
            stringa = re.sub('[\-\,\.\s\t\n\/]+', ' ', stringa)
            # 1)
            if (re.compile(r'\d{1,4}[\,\.\s\t\n]+bc', re.IGNORECASE).match(match.group())):
                date = format_data.pattern_1(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 2)
            if (re.compile(r'\d{4}[-.\/]\d{1,2}[-.\/]\d{1,2}', re.IGNORECASE).match(match.group())):
                date = format_data.pattern_2(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 3)
            if (re.compile(r'\d{1,2}[-.\/]\d{1,2}[-.\/]\d{4}', re.IGNORECASE).match(match.group())):
                date = format_data.pattern_3(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 4)
            if (re.compile(r'\d{1,2}[-.\/]\d{1,2}[-.\/]\d{2}', re.IGNORECASE).match(match.group())):
                date = format_data.pattern_4(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue

            # 5)
            if (re.compile(
                    r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{1,2}[\.\,\s\t\n\/]+\d{4}',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_5(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue

            # 6)
            if (re.compile(
                    r'\d{1,2}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{4}',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_6(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 7)
            if (re.compile(
                    r'\d{4}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\t\n\/]+\d{1,2}',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_7(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 8)
            if (re.compile(
                    r'\d{4}[\.\,\s\t\n\/]+\d{1,2}[\.\,\s\t\n\/]+(january|february|march|april|may|june|july|august|september|october|november|december)',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_8(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 9)
            if (re.compile(
                    r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\n\t\/]+\d{4}',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_9(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 10)
            if (re.compile(
                    r'\d{4}[\.\,\s\n\t\/]+(january|february|march|april|may|june|july|august|september|october|november|december)',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_10(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 11)
            if (re.compile(r'\d{1,2}(th|st)?[\.\,\s\t\n]+(century|Century)',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_11(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 12)
            if (re.compile(r'[0-9][0-9][0-9]0s',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_12(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 13)
            if (re.compile(r'(summer|winter|spring|autumn)[\.\,\s\t\n]+\d{4}',
                        re.IGNORECASE).match(match.group())):
                date = format_data.pattern_13(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 14)
            if (re.compile(r'\d{4}[\.\,\s\t\n]+(summer|winter|spring|autumn)',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_14(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 15)
            if (re.compile(r'(summer|winter|spring|autumn)[\.\,\s\t\n]+of[\.\,\s\t\n]+\d{4}',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_15(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 16)
            if (re.compile(r'\d{4}[\.\,\s\t\n]+of[\.\,\s\t\n]+(summer|winter|spring|autumn)',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_16(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 17)
            if (re.compile(r'(january|february|march|april|may|june|july|august|september|october|november|december)[\.\,\s\n\t]+of[\.\,\s\n\t]+\d{4}',
                            re.IGNORECASE).match(match.group())):
                date = format_data.pattern_17(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 18)
            if (re.compile(r'\d{4}[\.\,\s\n\t]+of[\.\,\s\n\t]+(january|february|march|april|may|june|july|august|september|october|november|december)',
                    re.IGNORECASE).match(match.group())):
                date = format_data.pattern_18(stringa)
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 19)
            if (re.compile(r'\d{4}',
                            re.IGNORECASE).match(match.group())):
                date = match.group()
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 20)
            #if (re.compile(r'(january|february|march|april|may|june|july|august|september|october|november|december)',
            #                re.IGNORECASE).match(match.group())):
            #    month_number = format_data.months[match.group().lower()]
            #    data = Date('XXXX-'+month_number,match.span())
            #   dates.append(data)
            #   continue
            # 21)
            if (re.compile(r'(summer|winter|spring|autumn)',
                        re.IGNORECASE).match(match.group())):
                season_number = format_data.seasons[match.group().lower()]
                year = datetime.datetime.now().year
                year = str(year)
                data = Date(year+'-' + season_number, match.span())
                dates.append(data)
                continue
            # 22)
            if (re.compile(r'(monday|tuesday|wednesday|thursday|friday|saturday|sunday)',
                            re.IGNORECASE).match(match.group())):
                day_number = format_data.days[match.group().lower()]
                data = Date('XXXX-WXX-' + day_number, match.span())
                dates.append(data)
                continue

    def date_keyword_pattern(self,text,dates):

        pattern = r'([^\w](now)[^\w]|(^now)|(now$))|' \
                  r'([^\w](current)[^\w]|(^current)|(current$))|' \
                  r'([^\w](present)[^\w]|(^present)|(present$))|' \
                  r'([^\w](currently)[^\w]|(^currently)|(currently$))|' \
                  r'([^\w](future)[^\w]|(^future)|(future$))|' \
                  r'([^\w](today)[^\w]|(^today)|(today$))|' \
                  r'([^\w](the[\.\,\s\t\n]+day)[^\w]|(^the[\.\,\s\t\n]+day)|(the[\.\,\s\t\n]+day$))|' \
                  r'([^\w](once)[^\w]|(^once)|(once$))|' \
                  r'([^\w](past)[^\w]|(^past)|(past$))|' \
                  r'([^\w](recently)[^\w]|(^recently)|(recently$))|' \
                  r'([^\w](previously)[^\w]|(^previously)|(previously$))'

        matches = self.make_matching(text, pattern)

        for match in matches:
            # 1) 2) 3) 4)
            if (re.compile(r'([^\w](now)[^\w]|(^now)|(now$))|'
                           r'([^\w](current)[^\w]|(^current)|(current$))|'
                           r'([^\w](present)[^\w]|(^present)|(present$))|'
                           r'([^\w](currently)[^\w]|(^currently)|(currently$))',
                           re.IGNORECASE).match(match.group())):
                date = "PRESENT_REF"
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 5)
            if (re.compile(r'([^\w](future)[^\w]|(^future)|(future$))',
                            re.IGNORECASE).match(match.group())):
                date = "FUTURE_REF"
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 6) 7)
            if (re.compile(r'([^\w](today)[^\w]|(^today)|(today$))|'
                            r'([^\w](the[\.\,\s\t\n]+day)[^\w]|(^the[\.\,\s\t\n]+day)|(the[\.\,\s\t\n]+day$))',
                            re.IGNORECASE).match(match.group())):
                current_date = datetime.datetime.now()
                year = str(current_date.year)
                month = str(current_date.month)
                day = str(current_date.day)
                if (len(month) == 1):
                    month = '0' + month
                if (len(day) == 1):
                    day = '0' + day
                date = year + '-' + month + '-' + day
                data = Date(date, match.span())
                dates.append(data)
                continue
            # 8) 9) 10)
            if (re.compile(r'([^\w](once)[^\w]|(^once)|(once$))|'
                           r'([^\w](past)[^\w]|(^past)|(past$))|'
                           r'([^\w](recently)[^\w]|(^recently)|(recently$))|'
                           r'([^\w](previously)[^\w]|(^previously)|(previously$))',
                           re.IGNORECASE).match(match.group())):
                date = "PAST_REF"
                data = Date(date, match.span())
                dates.append(data)
                continue