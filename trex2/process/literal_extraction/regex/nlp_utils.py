from nltk.corpus import stopwords
import string
import re

def remove_punctuations(s):
    return s.translate(str.maketrans("", "", string.punctuation))

def remove_extra_spaces(s):
    return " ".join(s.split())

def remove_words(tokens):
    stops = set(stopwords.words("english"))
    return [word for word in tokens if word not in stops]

def retain_alpha_nums(s):
    return re.sub(r'[^a-zA-Z0-9]', ' ', s)