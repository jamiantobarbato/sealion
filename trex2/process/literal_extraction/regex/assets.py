import os
import app.settings


# get assets from private folder

def get_text(path):
    p = os.path.join(app.settings.BASE_DIR,
                     os.path.join('trex2', os.path.join('process', os.path.join('static', os.path.join('literal_extraction', path)))))
    with open(p, 'r') as asset:
        data = asset.read()

    return data