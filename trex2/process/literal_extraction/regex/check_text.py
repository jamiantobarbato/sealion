import re
from trex2.process.literal_extraction.regex import assets

#class taken from MantisTable
class CheckText:
    def check_text(self,text):

        concepts = []
        validator = Validator(assets.get_text("currency.txt"))
        map = [
            # type, predicate
            ('url', validator.is_url),
            ('email', validator.is_email),
            ('GeoCoordinates' , validator.is_geocoords),
            ('address', validator.is_address),
            ('hexColor', validator.is_hexcolor),
            ('iso8601', validator.is_iso8601),
            ('boolean', validator.is_boolean),
            ('isbn', validator.is_isbn),
            ('ip', validator.is_ip),
            ('image', validator.is_image),
            ('currency', validator.is_currency),
            ('numeric', validator.is_numeric),
            ('creditcard', validator.is_creditcard),
        ]

        for (type,predicate) in map:
            matches = predicate(text)
            for match in matches:
                if (type != 'currency' and type != 'iata'):
                    concept = Date_concepts(type,text[match.start():match.end()],match.span())
                    concepts.append(concept)
                else:
                    if ((text[match.start():match.end()] is not '')):
                        concept = Date_concepts(type, text[match.start():match.end()], match.span())
                        concepts.append(concept)
        return concepts

#Model created to save Literals Results
class Date_concepts:

    def __init__(self,string_concept,word,boundary):
        #concept found (numeric, geocoordinate, ...)
        self.string_concept = string_concept
        #word extracted
        self.word = word
        #literal's boundary
        self.boundary = boundary
        #annotator
        self.annotator = "Mantis"


class Validator:
    def __init__(self, currency_dataset):
        self.currency ="".join(currency_dataset.split('\r')).split('\n')

    def apply_regex(self,reg,s):

        pattern = re.compile(reg)
        matches = pattern.finditer(s)
        return matches

    def is_geocoords(self, s):
        reg = r"[0-9]+°[0-9]+(′|')[0-9]+(″|''|′′)\s*N\s*[0-9]+°[0-9]+(′|')[0-9]+(″|''|′′)\s*E|" \
              r"[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)"

        return self.apply_regex(reg,s)

    def is_email(self, s):
        reg = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"

        return self.apply_regex(reg,s)

    def is_address(self, s):
        reg = r"\d+\s+\w+\s+(?:st(?:\.|reet)?|ave(?:nue)?|lane|dr(?:\.|ive)?)|" \
              r"^[\d]+[A-z\s,]+[\d]"

        return self.apply_regex(reg,s)

    def is_hexcolor(self, s):
        reg = r'#(?:[0-9a-fA-F]{3}){1,2}'

        return self.apply_regex(reg, s)

    def is_url(self, s):
        reg = r"(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?"

        return self.apply_regex(reg,s)

    def is_iso8601(self, s):
        reg = r'(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?'

        return self.apply_regex(reg, s)

    def is_boolean(self, s):
        reg = "([^\w](yes)[^\w]|(^yes)|(yes$))|" \
              "([^\w](no)[^\w]|(^no)|(no$))|" \
              "([^\w](false)[^\w]|(^false)|(false$))|" \
              "([^\w](true)[^\w]|(^true)|(true$))"

        return self.apply_regex(reg, s)

    def is_isbn(self, s):
        reg = r"(?:[0-9]{3}-)?[0-9]{1,5}-[0-9]{1,7}-[0-9]{1,6}-[0-9]"

        return self.apply_regex(reg, s)

    def is_ip(self, s):
        reg = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:\d{1,5})?|" \
              r"[0-9A-Za-z]{0,4}[\s\t\n]*:[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*:[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*(:)?[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*(:)?[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*(:)?[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*(:)?[\s\t\n]*[0-9A-Za-z]{0,4}[\s\t\n]*(:)?[\s\t\n]*[0-9A-Za-z]{0,4}"

        return self.apply_regex(reg, s)

    def is_image(self, s):
        reg = r'(([-\w]+\/)*[-\w]+\.(?:jpg|gif|png|jpeg|bmp|tiff))'

        return self.apply_regex(reg, s)

    def is_numeric(self, s):
        reg = r'\d+\,\d+|\d+\.\d+|\d+'

        return self.apply_regex(reg, s)

    def is_creditcard(self, s):
        reg = r'\d{4}[-\s]?\d{4}[-\s]?\d{4}[-\s]?\d{2,4}'

        return self.apply_regex(reg, s)

    def is_currency(self, s):
        reg = currency_reg(self.currency)

        return self.apply_regex(reg, s)

def currency_reg(currency):
    reg_tmp = ''
    for current in currency:
        reg_tmp += '([^\w]('+current+')[^\w]|(^'+current+')|('+current+'$))|'
    reg = r''+reg_tmp+''
    return reg
