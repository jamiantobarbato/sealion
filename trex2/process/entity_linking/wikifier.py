# -*- coding: utf-8 -*-

import json
import requests
from .entity_model import Entity,BasePipeline


# Wikifier entity linker
"""
        :param lang: Lang of the document to process
        :param threshold: threshold used to find Entities
        :param annotator: name of annotator (Wikifier)
        """
class WikifierEntityLinker:

    #init method for Wikifier
    def __init__(self,lang="en", threshold=0.8):
        self.lang=lang
        self.threshold=threshold
        self.annotator = "Wikifier"

    #HTTP POST request method to Wikifier
    def Http_Post_request(self,url,values):
        # Http post to Wikifier
        r = requests.get(url, params=values)
        # end of HTTP POST request
        return r.text

    def run(self,document):

        try:
            #set url and values for request to Wikifier
            url = 'http://www.wikifier.org/annotate-article'
            values = {'userKey': 'bkgzflqkfbnqnzehmuvodlxdsaibin',
                            'text': document.text,
                            'lang': self.lang,
                            'secondaryAnnotLanguage' : 'false',
                            'pageRankSqThreshold' : self.threshold,
                            'applyPageRankSqThreshold' : 'true',
                            'nTopDfValuesToIgnore' : '200',
                            'nWordsToIgnoreFromList' : '200',
                            'wikiDataClasses' : 'false',
                            'wikiDataClassIds' : 'false',
                            'support' : 'true',
                            'ranges' : 'true',
                            'includeCosines' : 'false',
                            'maxMentionEntropy' : '3'}

            response = self.Http_Post_request(url,values)

            #Json parse for Entity
            json_converter = json.loads(response)
            # take from JSON: uri,(start,end), title
            for i in json_converter["annotations"]: #Take annotations list of object i json
                boundaries = i["support"] # take the first word matching in the setence
                all_boundaries = []
                for j in boundaries:
                    if (j['prbConfidence'] > 0.5):
                        start_end = (j["chFrom"], j["chTo"] + 1)
                        all_boundaries.append(start_end)
                entity = Entity(
                    uri = i['dbPediaIri'], #uri dbpedia
                    boundaries = all_boundaries, #[(start,end),(start,end)]
                    surfaceform = i["title"], #concept
                    annotator=self.annotator) #annotator name

                #Add Etity to document.entities list
                document.entities.append(entity)

        except Exception as e:
            print ("")

        #return document
        return document