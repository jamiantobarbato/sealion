from .models import *

from trex2.process.entity_linking.entity_model import Document, Entity
from trex2.process.literal_extraction.regex.check_text import Date_concepts
from trex2.process.data_preparation.data_preparation import DataPreparation
from trex2.process.data_preparation.coreference.coreference_trex import SimpleCoreference

#calculate and save data preparation results
def save_dp(abstract):
    print(abstract)
    dp = DataPreparation(abstract.text)
    #calculate data preparation
    stats = dp.normalize()  ###stats[0] contiene il testo normalizato
                            ###stats[1] contiene normalize norm che è un lista

    print("abstract.concept == " + abstract.concept)

    new_text = dp.separate_sentence_at_pronoun(stats[0])  ####[Matt]
    new_text = dp.delete_subsentence_without_coreference(new_text) ####[Matt]

    #SimpleCoreference
    coreference = SimpleCoreference()

    new_text = coreference.find_and_replace_pronouns(new_text, abstract.concept)

    #create object DPProcess to save in MongoDB
    p = DPProcess(
        output=new_text,
        stats=[DPStats(msg=s[0], applied=s[1], boundaries=get_boundaries(s[2])) for s in stats[1]]
    )
    p.save()

    abstract.lexprocess.dp = p
    abstract.lexprocess.dp.save()
    abstract.lexprocess.save()

    abstract.percentage = 25

    abstract.is_done_dp = True
    abstract.save()

    abstract_to_link = abstract.lexprocess.dp
    boundaries_list = []
    for a in abstract_to_link.stats:
        for boundary in a.boundaries:
            boundaries_list.append((boundary.start_char, boundary.end_char))

    return boundaries_list

#get data preparation results from DB
def get_dp(abstract):
    abstract_to_link = abstract.lexprocess.dp
    boundaries_list = []
    #create list of boundaries
    for a in abstract_to_link.stats:
        for boundary in a.boundaries:
            boundaries_list.append((boundary.start_char, boundary.end_char))

    return boundaries_list

#save entity linking results
def save_el(abstract, document):
    entities = []
    #create list of EntityModel
    for d in document.entities:
        entity = EntityModel(
            uri=d.uri,
            surfaceform=d.surfaceform,
            annotator=d.annotator,
            img=d.img,
            text=d.text,
            in_all=d.in_all,
            boundaries=get_boundaries(d.boundaries)
        )
        entities.append(entity)
    #create ELProcess object
    el = ELProcess(
        entity_linking=entities
    )

    el.save()

    abstract.lexprocess.el = el
    abstract.lexprocess.el.save()
    abstract.lexprocess.save()
    abstract.percentage = 50

    abstract.is_done_el = True
    abstract.save()
#get results of entity linking from DB
def get_el(abstract):
    entities = abstract.lexprocess.el

    entity_list = []
    for entity in entities.entity_linking:
        boundary_list = []
        #create list of Boundaries about one entity
        for boundary in entity.boundaries:
            boundary_list.append((boundary.start_char, boundary.end_char))
        #create entity --> Entity(uri, boundaries=[(s,e),(s,e),(s,e)], surfaceform, text, img, annotator, in_all)
        e = Entity(
            uri=entity.uri,
            boundaries=boundary_list,
            surfaceform=entity.surfaceform,
            text=entity.text,
            img=entity.img,
            annotator=entity.annotator,
            in_all=entity.in_all
        )
        entity_list.append(e)

    return entity_list


#save literal extraction results 
def save_le(abstract, document, type):
    dates = []
    for data in document.entities:
        #create Boundary object
        boundary = Boundary(
            start_char=data.boundaries[0],
            end_char=data.boundaries[1]
        )
        #create EntityModel object (used also for Dates)
        literal = EntityModel(
            uri=data.uri,
            boundaries=[boundary],
            surfaceform=data.surfaceform,
            annotator=data.annotator
        )
        #list of dates
        dates.append(literal)

    literals = []
    for literal in type:
        #create Boundary object
        boundary = Boundary(
            start_char=literal.boundary[0],
            end_char=literal.boundary[1]
        )
        #create EntityModel object (used also for Literals)
        lit = EntityModel(
            uri=literal.string_concept,
            boundaries=[boundary],
            surfaceform=literal.word,
            annotator=literal.annotator
        )
        #list of literals
        literals.append(lit)
    #create LEProcess object
    le = LEProcess(
        date_linker=dates,
        regex=literals
    )

    le.save()

    abstract.lexprocess.le = le
    abstract.lexprocess.le.save()
    abstract.lexprocess.save()

    abstract.percentage = 75

    abstract.is_done_le = True
    abstract.save()

#get literal extraction result from DB
def get_le(abstract):
    dates = abstract.lexprocess.le.date_linker
    abstractToLink = abstract.lexprocess.dp

    #create dates list of object
    date_list = []
    for data in dates:
        entity = Entity(
            uri=data.uri,
            surfaceform=data.surfaceform,
            annotator=data.annotator,
            boundaries=(data.boundaries[0].start_char, data.boundaries[0].end_char)
        )
        date_list.append(entity)

    type = abstract.lexprocess.le.regex

    #create literals list of object
    regex_list = []
    for reg in type:
        data_concept = Date_concepts(
            string_concept=reg.uri,
            word=reg.surfaceform,
            boundary=(reg.boundaries[0].start_char, reg.boundaries[0].end_char)
        )
        regex_list.append(data_concept)

    document = Document(abstract.uri, abstract.concept, abstract.uri, abstractToLink.output, entities=date_list)

    return (document, regex_list)

#save relation extraction results
def save_re(abstract, sentences):
    text_sentences = []
    for sentence in sentences:
        predicates = []
        triples = []
        #create and save Triple objects
        for triple in sentence.triples:
            tr = TripleModel(
                subject_uri=triple.subject_uri,
                subject_ontology=triple.subject_ontology,
                subject=triple.subject,
                predicate_uri=triple.predicate_uri,
                predicate=triple.predicate,
                object_uri=triple.object_uri,
                object_ontology=triple.object_ontology,
                object=triple.object
            )
            triples.append(tr)
        #create and save Predicate objects
        for predicate in sentence.predicates:
            pr = PredicateModel(
                predicate=predicate.predicate,
                start_char=predicate.start_char,
                end_char=predicate.end_char
            )
            predicates.append(pr)
        #create and save SentenceModel objects
        sentence_model = SentenceModel(
            sentence=sentence.sentence,
            triples=triples,
            predicates=predicates
        )
        text_sentences.append(sentence_model)
    
    #create REProcess object
    re = REProcess(
        sentences=text_sentences
    )

    re.save()

    abstract.lexprocess.re = re
    abstract.lexprocess.re.save()
    abstract.lexprocess.save()
    abstract.percentage = 100

    abstract.is_done_re = True
    abstract.save()

# return the list of boundaries about data-preparation to store them in MongoDB
def get_boundaries(list_of_boundaries):
    boundaries_list_final = []
    for boundary in list_of_boundaries:
        new_boundary = Boundary(
            start_char=boundary[0],
            end_char=boundary[1]
        )
        boundaries_list_final.append(new_boundary)
    return boundaries_list_final