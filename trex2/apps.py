from django.apps import AppConfig


class Trex2Config(AppConfig):
    name = 'trex2'

    def ready(self):
        import trex2.signals
