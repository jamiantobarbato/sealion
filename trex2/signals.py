from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Abstract, LexProcess

@receiver(post_save, sender=Abstract)
def create_lexprocess(sender, instance, created, **kwargs):
    if created:
        LexProcess.objects.get_or_create(abstract=instance) 
