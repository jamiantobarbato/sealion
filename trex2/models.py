from django import forms
from django.db import models
from django.utils.timezone import now
from djongo import models as mongo_models

# Create your models here.
class Abstract(models.Model):
    uri = models.URLField()
    concept = models.CharField(null=True, max_length=200)
    text = models.TextField(null=True)
    pub_date = models.DateTimeField(default=now)
    percentage = models.PositiveIntegerField(null=False, default=0)
    is_done_dp=models.BooleanField(default=False)
    is_done_el=models.BooleanField(default=False)
    is_done_le=models.BooleanField(default=False)
    is_done_re=models.BooleanField(default=False)

class Boundary(models.Model):
    start_char = models.PositiveIntegerField(null=False, default=0)
    end_char = models.PositiveIntegerField(null=False, default=0)

class BoundaryForm(forms.ModelForm):
    class Meta:
        model = Boundary
        fields = (
            'start_char',
            'end_char'
        )

class EntityModel(models.Model):
    uri = models.URLField()
    surfaceform = models.CharField(null=True, max_length=200)
    img = models.CharField(null=True, max_length=500)
    text = models.TextField(default=None)
    annotator = models.CharField(null=True, max_length=200)
    in_all = models.BooleanField(default=False)
    boundaries = mongo_models.ArrayModelField(
        model_container=Boundary,
        model_form_class=BoundaryForm,
        default=[]
    )

class EntityModelForm(forms.ModelForm):
    class Meta:
        model = EntityModel
        fields=(
            'uri',
            'surfaceform',
            'annotator',
            'in_all',
            'boundaries'
        )

class DPStats(models.Model):
    msg = models.CharField(max_length=200, blank=True)
    applied = models.PositiveIntegerField(null=False, default=0)
    boundaries = mongo_models.ArrayModelField(
        model_container = Boundary,
        model_form_class = BoundaryForm,
        default = []
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.msg


class DPStatsForm(forms.ModelForm):
    class Meta:
        model = DPStats
        fields = (
            'msg',
            'applied',
            'boundaries'
        )

class DPProcess(models.Model):
    output = models.TextField(null=True)
    stats = mongo_models.ArrayModelField(
        model_container=DPStats,
        model_form_class=DPStatsForm,
        default=[]
    )

class ELProcess(models.Model):
    entity_linking = mongo_models.ArrayModelField(
        model_container = EntityModel,
        model_form_class = EntityModelForm,
        default = []
    )

class LEProcess(models.Model):
    date_linker = mongo_models.ArrayModelField(
        model_container=EntityModel,
        model_form_class=EntityModelForm,
        default = []
    )
    regex = mongo_models.ArrayModelField(
        model_container=EntityModel,
        model_form_class=EntityModelForm,
        default = []
    )

class PredicateModel(models.Model):
    predicate = models.CharField(max_length=200, blank=True)
    start_char = models.PositiveIntegerField(null=False, default=0)
    end_char = models.PositiveIntegerField(null=False, default=0)

class PredicateModelForm(forms.ModelForm):
    class Meta:
        model = PredicateModel
        fields = (
            'predicate',
            'start_char',
            'end_char'
        )

class TripleModel(models.Model):
    subject_uri = models.CharField(max_length=300, blank=True)
    subject_ontology = models.CharField(max_length=300, blank=True)
    subject = models.CharField(max_length=200, blank=True)
    predicate_uri = models.CharField(max_length=300, blank=True)
    predicate = models.CharField(max_length=200, blank=True)
    object_uri = models.CharField(max_length=300, blank=True)
    object_ontology = models.CharField(max_length=300, blank=True)
    object = models.CharField(max_length=200, blank=True)

class TripleModelForm(forms.ModelForm):
    class Meta:
        model = TripleModel
        fields = (
            'subject_uri',
            'subject_ontology',
            'subject',
            'predicate_uri',
            'predicate',
            'object_uri',
            'object_ontology',
            'object'
        )

class SentenceModel(models.Model):
    sentence = models.TextField(null=True)
    triples = mongo_models.ArrayModelField(
        model_container=TripleModel,
        model_form_class=TripleModelForm,
        default=[]
    )
    predicates = mongo_models.ArrayModelField(
        model_container=PredicateModel,
        model_form_class=PredicateModelForm,
        default=[]
    )

class SentenceModelForm(forms.ModelForm):
    class Meta:
        model = SentenceModel
        fields = (
            'sentence',
            'triples',
            'predicates'
        )

class REProcess(models.Model):
    sentences = mongo_models.ArrayModelField(
        model_container=SentenceModel,
        model_form_class=SentenceModelForm,
        default=[]
    )

class LexProcess(models.Model):
    abstract = models.OneToOneField(
        Abstract,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='lexprocess'
    )
    dp = models.ForeignKey(
        DPProcess,
        on_delete=models.CASCADE,
        null=True,
    )
    el = models.ForeignKey(
        ELProcess,
        on_delete=models.CASCADE,
        null=True
    )
    le = models.ForeignKey(
        LEProcess,
        on_delete=models.CASCADE,
        null=True
    )
    re = models.ForeignKey(
        REProcess,
        on_delete=models.CASCADE,
        null=True
    )
