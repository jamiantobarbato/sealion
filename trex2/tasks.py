from celery import shared_task
from celery import task, current_task
from trex2.models import Abstract, LexProcess

@shared_task
def delete_all_abstracts():
    l = Abstract.objects.all().count()
    i = 0
    for x in Abstract.objects.all().iterator():
        i += 1
        x.delete()
        process_percent = int(100 * float(i) / float(l))
        current_task.update_state(state='PROGRESS',
                                meta={
                                    'current': process_percent,
                                    'msg': x.concept
                                })
